# Workshop Kubernetes
### by Settakit

#### Prepare environment
##### 1. Install Docker Desktop
- [Winodows](https://docs.docker.com/docker-for-windows/install/) 
- [Mac](https://docs.docker.com/docker-for-mac/install/)
---
##### 2. Enable Kubernetes Cluster on Docker Desktop [link](https://docs.docker.com/desktop/kubernetes/)
** ถ้าไม่สามารถ Enable Kubernetes ได้ (รอ Kubenetes Start นานเกิน 10 นาที) ให้ทำการ Uninstall Docker และ Install Docker ใหม่อีกครั้ง
---
##### 3. Install kubectl command
**ตรวจสอบว่าเครื่อง Computer มี kubectl command อยู่หรือไม่ ถ้ามีให้ข้ามขั้นตอนนี้ไปได้เลย
``` sh
kubectl version --short
```
- [Windows](https://medium.com/@ggauravsigra/install-kubectl-on-windows-af77da2e6fff)
- Mac
``` sh
brew install kubectl
kubectl version --short
```
---
##### 4. Install siege command for Load Test (for LAB-2 Autoscaling - HPA)
- Mac
``` sh
brew install siege
```
- [Windows](https://github.com/ewwink/siege-windows)
---
##### LAB-1
- Clone Repository and go to k8s-101-workshop folder
``` sh
git clone https://gitlab.com/settakit.t/k8s-101-workshop.git
cd k8s-101-workshop
```
- Create lab-1 Namespace
``` sh
kubectl create ns lab-1
```
- Create Standalone Pod
``` sh
kubectl create -f lab-1/pod.yaml -n lab-1
```
- Create ClusterIP Service
``` sh
kubectl create -f lab-1/svc-clusterip.yaml -n lab-1
```
- Create Loadbalancer Service
``` sh
kubectl create -f lab-1/svc-loadbalancer.yaml -n lab-1
```
- Create NodePort Service
``` sh
kubectl create -f lab-1/svc-nodeport.yaml -n lab-1
```
- Delete Pod
``` sh
kubectl delete po <POD_NAME> -n lab-1
```
- Create Deployment
``` sh
kubectl create -f lab-1/deployment.yaml -n lab-1
```
---
##### LAB-2
- Install Ingress Controller
``` sh
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.11.2/deploy/static/provider/cloud/deploy.yaml
```
- Create Secret TLS CERT
``` sh
# Create Key and Cert
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout workshop/ingress/ingress.key -out workshop/ingress/ingress.cert -subj "/CN=example.com/O=example.com"

# If cannot crate
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout workshop/ingress/ingress.key -out workshop/ingress/ingress.cert -subj "/CN=example.com/O=example.com" -config workshop/config.conf

```
- Deploy Kubernetes Dashboard
``` sh
# Install Helm
# MAC OS
brew install helm

# Deploy K8s Dashboard
# Add kubernetes-dashboard repository
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
# Deploy a Helm Release named "kubernetes-dashboard" using the kubernetes-dashboard chart
helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --create-namespace --namespace kubernetes-dashboard

# Create Secret TLS
kubectl create secret tls ingress-tls --key workshop/ingress/ingress.key --cert workshop/ingress/ingress.cert -n kubernetes-dashboard

# Create Ingress for Access to Dashboard
kubectl create -f lab-2/k8s-dashboard/ingress-dashboard.yaml

# Create Service Account and Secret for Login Kubernetes Dashboard
kubectl create -f lab-2/k8s-dashboard/admin-user.yaml

# Get Token to Login
kubectl get secret admin-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d
```
- Create lab-2 Namespace
``` sh
kubectl create ns lab-2
```
- Create ConfigMap for PostgreSQL
``` sh
kubectl create -f lab-2/postgres-schema-configmap.yaml -n lab-2
```
- Create Secret for Postgres
``` sh
kubectl create -f lab-2/postgres-secret.yaml -n lab-2
```
- Create PVC for Postgres
``` sh
kubectl create -f lab-2/postgres-pvc.yaml -n lab-2
```
- Create Postgres Deployment
``` sh
kubectl create -f lab-2/postgres.yaml -n lab-2
```
- Create nginx Deplment and Service for Ingress and HPA
``` sh
kubectl create -f lab-2/deployment.yaml -n lab-2
```
- Create Ingress for nginx
``` sh
kubectl create -f lab-2/ingress-nginx.yaml -n lab-2
```
- Deploy Metric Server
``` sh
kubectl create -f workshop/metric-server/metric-server.yaml
```
- Create HPA for nginx
``` sh
kubectl create -f lab-2/hpa-cpu.yaml -n lab-2
```
- Load Test
```sh
siege <URL>
```
---
##### Start Workshop
- Create Namespace
``` sh
kubectl create namespace workshop
```
- Create ConfigMap for API Service
``` sh
kubectl create -f workshop/api-configmap.yaml -n workshop
```
- Create Secret Username and Password DB
``` sh
kubectl create -f workshop/api-secret.yaml -n workshop
```

- Create PostgreSQL Service for use API Calls
``` sh
kubectl create -f lab-2/postgres-svc.yaml -n lab-2
```

- Deploy API Service with Service ClusterIP and NodePort
``` sh
# API Service
kubectl create -f workshop/api/api-deployment.yaml -f workshop/api/api-svc.yaml -n workshop
```
- Create Ingress for API Service
``` sh
kubectl create -f workshop/ingress/api-service-ingress.yaml -n workshop
```
- Create Ingress TLS for API Service
``` sh
kubectl create -f workshop/ingress/api-service-ingress-tls.yaml -n workshop
```

- Deploy Web Frontend
``` sh
kubectl create -f workshop/web/web-deployment.yaml -n workshop
```

- Create Service for Web Frontend
``` sh
kubectl create -f workshop/web/web-svc.yaml -n workshop
```

- Create Ingress and Ingress-TLS for Web Frontend
``` sh
# Ingress
kubectl create -f workshop/ingress/web-ingress.yaml -n workshop

# Ingress-TLS
kubectl create -f workshop/ingress/web-ingress-tls.yaml -n workshop
```

- Create HPA for API and Web Frontend
``` sh
# API
kubectl create -f workshop/hpa/api-service-hpa-cpu.yaml -n workshop

# Web Frontend
kubectl create -f workshop/hpa/web-hpa-cpu.yaml -n workshop
```
---
## License

Sirisoft Company

[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)

   [dill]: <https://github.com/joemccann/dillinger>
   [git-repo-url]: <https://github.com/joemccann/dillinger.git>
   [john gruber]: <http://daringfireball.net>
   [df1]: <http://daringfireball.net/projects/markdown/>
   [markdown-it]: <https://github.com/markdown-it/markdown-it>
   [Ace Editor]: <http://ace.ajax.org>
   [node.js]: <http://nodejs.org>
   [Twitter Bootstrap]: <http://twitter.github.com/bootstrap/>
   [jQuery]: <http://jquery.com>
   [@tjholowaychuk]: <http://twitter.com/tjholowaychuk>
   [express]: <http://expressjs.com>
   [AngularJS]: <http://angularjs.org>
   [Gulp]: <http://gulpjs.com>

   [PlDb]: <https://github.com/joemccann/dillinger/tree/master/plugins/dropbox/README.md>
   [PlGh]: <https://github.com/joemccann/dillinger/tree/master/plugins/github/README.md>
   [PlGd]: <https://github.com/joemccann/dillinger/tree/master/plugins/googledrive/README.md>
   [PlOd]: <https://github.com/joemccann/dillinger/tree/master/plugins/onedrive/README.md>
   [PlMe]: <https://github.com/joemccann/dillinger/tree/master/plugins/medium/README.md>
   [PlGa]: <https://github.com/RahulHP/dillinger/blob/master/plugins/googleanalytics/README.md>
